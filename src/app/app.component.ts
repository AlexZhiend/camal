import { Menu } from './_model/menu';
import { LoginService } from './_service/login.service';
import { MenuService } from './_service/menu.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  menus: Menu[]=[];
  title = 'proyectocamal';
  
  constructor(private menuService:MenuService,public loginService:LoginService){
  }
  ngOnInit(){
    this.menuService.menuCambio.subscribe(data=>{
      this.menus=data;
    })
  }
}
