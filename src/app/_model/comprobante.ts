import { DetalleSacrificio } from './detallesacrificio';
export class Comprobante {
    idcomprobante:number;
    fecha:Date;
    numerocomprobante:number;
    ruc:string;
    cliente:string;
    detallesacrificio:DetalleSacrificio[];
    otros:number;
    descripcionotros:string;
    total:number;
    cancelado:number;
    deuda:number;
}