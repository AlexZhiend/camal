import { Comprobante } from './comprobante';
import { Animal } from './animal';
export class DetalleSacrificio {
    iddetallesacrificio:number;
    cantidad:number;
    animal:Animal;
    comprobante:Comprobante;
    importe:number;
}