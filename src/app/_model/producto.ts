import { Proveedor } from './proveedor';
import { CategoriaProducto } from './categoriaproducto';
export class Producto {
    idproducto:number;
    nombreproducto:string;
    cantidadproducto:number;
    unidadproducto:string;
    pingresoproducto:number;
    marcaproducto:string;
    fingresoproducto:Date;
    categoriaproducto:CategoriaProducto;
    proveedor:Proveedor;   
}