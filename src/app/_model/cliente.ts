export class Cliente {
    idcliente:number;
    dni:string;
    apellidosynombres:string;
    edad:number;
    direccion:string;
    telefono:string;
    sexo:string;
}