import { Cliente } from './cliente';
import { CategoriaAnimal } from './categoriaanimal';
export class Animal {
    idanimal: number;
    descripcion:string;
    especie:string;
    sexo:string;
    cantidadanimal:number;
    categoriaanimal:CategoriaAnimal
    cliente:Cliente
    fingresoanimal:Date;
    estado:number;
}