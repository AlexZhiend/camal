export class Proveedor {
    idproveedor:number;
    razonsocialproveedor:string;
    descripcionproveedor:string;
    telefonoproveedor:string;
    direccionproveedor:string;
    correoproveedor:string;
    rucproveedor:string;
}