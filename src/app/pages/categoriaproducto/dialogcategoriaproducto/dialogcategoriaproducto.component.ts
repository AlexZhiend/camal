import { CategoriaproductoService } from './../../../_service/categoriaproducto.service';
import { CategoriaProducto } from './../../../_model/categoriaproducto';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogcategoriaproducto',
  templateUrl: './dialogcategoriaproducto.component.html',
  styleUrls: ['./dialogcategoriaproducto.component.css']
})
export class DialogcategoriaproductoComponent implements OnInit {

  categoriaproducto: CategoriaProducto;
  constructor(public dialogRef: MatDialogRef<DialogcategoriaproductoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CategoriaProducto,
    private CategoriaProductoService:CategoriaproductoService) { }

  ngOnInit() {

    this.categoriaproducto = new CategoriaProducto();
      this.categoriaproducto.idcategoria=this.data.idcategoria;
      this.categoriaproducto.descripcion = this.data.descripcion;
      if(this.data.idcategoria!= null){

      }
  
  }
operar(){
  
    if(this.categoriaproducto != null && this.categoriaproducto.idcategoria > 0){

      this.CategoriaProductoService.modificarCategoriaProducto(this.categoriaproducto).subscribe(data => {
        
          this.CategoriaProductoService.listarCategoriaProductol().subscribe(CategoriaProductos => {
            this.CategoriaProductoService.categoriaproductoCambio.next(CategoriaProductos);
            this.CategoriaProductoService.mensaje.next("Se modificó correctamente");
          });
        
      });
    }else{
      this.CategoriaProductoService.registrarCategoriaProducto(this.categoriaproducto).subscribe(data => {
        
          this.CategoriaProductoService.listarCategoriaProductol().subscribe(CategoriaProductos => {
            this.CategoriaProductoService.categoriaproductoCambio.next(CategoriaProductos);
            this.CategoriaProductoService.mensaje.next("Se registró correctamente");
          });
        
      });
    }
    this.dialogRef.close();
}

  cancelar(){
    this.dialogRef.close();
    this.CategoriaProductoService.mensaje.next('se canceló el procedimiento');
  }

}
