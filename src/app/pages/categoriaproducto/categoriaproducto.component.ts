import { DialogcategoriaproductoComponent } from './dialogcategoriaproducto/dialogcategoriaproducto.component';
import { CategoriaproductoService } from './../../_service/categoriaproducto.service';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { CategoriaProducto } from './../../_model/categoriaproducto';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-categoriaproducto',
  templateUrl: './categoriaproducto.component.html',
  styleUrls: ['./categoriaproducto.component.css']
})
export class CategoriaproductoComponent implements OnInit {
  lista: CategoriaProducto[]=[];
  displayedColumns=['idcategoria','descripcion','acciones'];
  dataSource: MatTableDataSource<CategoriaProducto>;
  elim:CategoriaProducto;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;

  constructor(private categoriaproductoService:CategoriaproductoService,public dialog:MatDialog,
    private matSnackBar:MatSnackBar,public route:ActivatedRoute) {
   }

   ngOnInit() {
    this.categoriaproductoService.categoriaproductoCambio.subscribe(data => {
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.categoriaproductoService.mensaje.subscribe(data => {
        this.matSnackBar.open(data, 'Aviso',{duration:2000});
      });
    });

    this.categoriaproductoService.listarCategoriaProductol().subscribe(data => {
      this.lista=data;
      console.log(data);
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });

  }

  applyFilter(filterValue: string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
  
  
  openDialog(categoriaproducto: CategoriaProducto): void {

    let pro = categoriaproducto != null ? categoriaproducto : new CategoriaProducto();
    let dialogRef = this.dialog.open(DialogcategoriaproductoComponent, {
      width: '500px',   
      disableClose: false,   
      data: pro      
    });
  }

  eliminar(){
    if (this.elim!=null && this.elim.idcategoria>0) {
      this.categoriaproductoService.eliminarCategoriaProducto(this.elim.idcategoria).subscribe(data=>{
        this.categoriaproductoService.mensaje.next('El categoria del producto se eliminó correctamente');
        this.categoriaproductoService.listarCategoriaProductol().subscribe(data =>{
          this.lista = data;
          this.dataSource = new MatTableDataSource(this.lista);
        });
      });
    }
    else{
      this.categoriaproductoService.mensaje.next('Ocurrió un error en el procedimiento')
    }
  }
  
  seleccionar(cat:CategoriaProducto){
    this.elim=cat;
  }
  
}
