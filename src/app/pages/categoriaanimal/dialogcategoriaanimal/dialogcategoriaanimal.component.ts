import { CategoriaanimalService } from './../../../_service/categoriaanimal.service';
import { CategoriaAnimal } from './../../../_model/categoriaanimal';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogcategoriaanimal',
  templateUrl: './dialogcategoriaanimal.component.html',
  styleUrls: ['./dialogcategoriaanimal.component.css']
})
export class DialogcategoriaanimalComponent implements OnInit {


  categoriaanimal: CategoriaAnimal;
  constructor(public dialogRef: MatDialogRef<DialogcategoriaanimalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CategoriaAnimal,
    private CategoriaAnimalService:CategoriaanimalService) { }

  ngOnInit() {

    this.categoriaanimal = new CategoriaAnimal();
      this.categoriaanimal.idcategoriaanimal=this.data.idcategoriaanimal;
      this.categoriaanimal.denominacion = this.data.denominacion;
      this.categoriaanimal.tamanio = this.data.tamanio;
      this.categoriaanimal.precio = this.data.precio;
      if(this.data.idcategoriaanimal!= null){

      }
  
  }
operar(){
  
    if(this.categoriaanimal != null && this.categoriaanimal.idcategoriaanimal > 0){

      this.CategoriaAnimalService.modificarCategoriaAnimal(this.categoriaanimal).subscribe(data => {
        
          this.CategoriaAnimalService.listarCategoriaAnimal().subscribe(CategoriaAnimals => {
            this.CategoriaAnimalService.categoriaanimalCambio.next(CategoriaAnimals);
            this.CategoriaAnimalService.mensaje.next("Se modificó correctamente");
          });
        
      });
    }else{
      this.CategoriaAnimalService.registrarCategoriaAnimal(this.categoriaanimal).subscribe(data => {
        
          this.CategoriaAnimalService.listarCategoriaAnimal().subscribe(CategoriaAnimals => {
            this.CategoriaAnimalService.categoriaanimalCambio.next(CategoriaAnimals);
            this.CategoriaAnimalService.mensaje.next("Se registró correctamente");
          });
        
      });
    }
    this.dialogRef.close();
}

  cancelar(){
    this.dialogRef.close();
    this.CategoriaAnimalService.mensaje.next('se canceló el procedimiento');
  }

}
