import { DialoganimalComponent } from './../animal/dialoganimal/dialoganimal.component';
import { CategoriaanimalService } from './../../_service/categoriaanimal.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { CategoriaAnimal } from './../../_model/categoriaanimal';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DialogcategoriaanimalComponent } from './dialogcategoriaanimal/dialogcategoriaanimal.component';

@Component({
  selector: 'app-categoriaanimal',
  templateUrl: './categoriaanimal.component.html',
  styleUrls: ['./categoriaanimal.component.css']
})
export class CategoriaanimalComponent implements OnInit {

  lista: CategoriaAnimal[]=[];
  displayedColumns=['idcategoriaanimal','denominacion','tamanio','precio','acciones'];
  dataSource: MatTableDataSource<CategoriaAnimal>;
  elim:CategoriaAnimal;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;

  constructor(private CategoriaAnimalService:CategoriaanimalService,public dialog:MatDialog,
    private matSnackBar:MatSnackBar,public route:ActivatedRoute) {
   }

   ngOnInit() {
    this.CategoriaAnimalService.categoriaanimalCambio.subscribe(data => {
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.CategoriaAnimalService.mensaje.subscribe(data => {
        this.matSnackBar.open(data, 'Aviso',{duration:2000});
      });
    });

    this.CategoriaAnimalService.listarCategoriaAnimal().subscribe(data => {
      this.lista=data;
      console.log(data);
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });

  }

  applyFilter(filterValue: string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
  
  
  openDialog(cat: CategoriaAnimal): void {

    let pro = cat != null ? cat : new CategoriaAnimal();
    let dialogRef = this.dialog.open(DialogcategoriaanimalComponent, {
      width: '500px',   
      disableClose: false,   
      data: pro      
    });
  }

  eliminar(){
    if (this.elim!=null && this.elim.idcategoriaanimal>0) {
      this.CategoriaAnimalService.eliminarCategoriaAnimal(this.elim.idcategoriaanimal).subscribe(data=>{
        this.CategoriaAnimalService.mensaje.next('El categoria del producto se eliminó correctamente');
        this.CategoriaAnimalService.listarCategoriaAnimal().subscribe(data =>{
          this.lista = data;
          this.dataSource = new MatTableDataSource(this.lista);
        });
      });
    }
    else{
      this.CategoriaAnimalService.mensaje.next('Ocurrió un error en el procedimiento')
    }
  }
  
  seleccionar(cat:CategoriaAnimal){
    this.elim=cat;
  }
  
}
