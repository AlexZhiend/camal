import { DialogclienteComponent } from './dialogcliente/dialogcliente.component';
import { ClienteService } from './../../_service/cliente.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { Cliente } from './../../_model/cliente';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  lista: Cliente[]=[];
  displayedColumns=['idcliente','dni','apellidosynombres','telefono','acciones'];
  dataSource: MatTableDataSource<Cliente>;
  elim:Cliente;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;

  constructor(private ClienteService:ClienteService,public dialog:MatDialog,
    private matSnackBar:MatSnackBar,public route:ActivatedRoute) {
   }

   ngOnInit() {
    this.ClienteService.clienteCambio.subscribe(data => {
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.ClienteService.mensaje.subscribe(data => {
        this.matSnackBar.open(data, 'Aviso',{duration:2000});
      });
    });

    this.ClienteService.listarCliente().subscribe(data => {
      this.lista=data;
      console.log(data);
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });

  }

  applyFilter(filterValue: string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
  
  
  openDialog(cli: Cliente): void {

    let pro = cli != null ? cli : new Cliente();
    let dialogRef = this.dialog.open(DialogclienteComponent, {
      width: '500px',   
      disableClose: false,   
      data: pro      
    });
  }

  eliminar(){
    if (this.elim!=null && this.elim.idcliente>0) {
      this.ClienteService.eliminarCliente(this.elim.idcliente).subscribe(data=>{
        this.ClienteService.mensaje.next('El categoria del producto se eliminó correctamente');
        this.ClienteService.listarCliente().subscribe(data =>{
          this.lista = data;
          this.dataSource = new MatTableDataSource(this.lista);
        });
      });
    }
    else{
      this.ClienteService.mensaje.next('Ocurrió un error en el procedimiento')
    }
  }
  
  seleccionar(cat:Cliente){
    this.elim=cat;
  }
  
}
