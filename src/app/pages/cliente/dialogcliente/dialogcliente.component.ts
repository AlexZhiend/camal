import { ClienteService } from './../../../_service/cliente.service';
import { Cliente } from './../../../_model/cliente';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogcliente',
  templateUrl: './dialogcliente.component.html',
  styleUrls: ['./dialogcliente.component.css']
})
export class DialogclienteComponent implements OnInit {

  cliente: Cliente;
  constructor(public dialogRef: MatDialogRef<DialogclienteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Cliente,
    private ClienteService:ClienteService) { }

  ngOnInit() {

    this.cliente = new Cliente();
      this.cliente.idcliente=this.data.idcliente;
      this.cliente.dni = this.data.dni;
      this.cliente.apellidosynombres = this.data.apellidosynombres;
      this.cliente.edad = this.data.edad;
      this.cliente.direccion = this.data.direccion;
      this.cliente.telefono = this.data.telefono;
      this.cliente.sexo = this.data.sexo;
      if(this.data.idcliente!= null){

      }
  
  }
operar(){
  
    if(this.cliente != null && this.cliente.idcliente > 0){

      this.ClienteService.modificarCliente(this.cliente).subscribe(data => {
        
          this.ClienteService.listarCliente().subscribe(Clientes => {
            this.ClienteService.clienteCambio.next(Clientes);
            this.ClienteService.mensaje.next("Se modificó correctamente");
          });
        
      });
    }else{
      this.ClienteService.registrarCliente(this.cliente).subscribe(data => {
        
          this.ClienteService.listarCliente().subscribe(Clientes => {
            this.ClienteService.clienteCambio.next(Clientes);
            this.ClienteService.mensaje.next("Se registró correctamente");
          });
        
      });
    }
    this.dialogRef.close();
}

  cancelar(){
    this.dialogRef.close();
    this.ClienteService.mensaje.next('se canceló el procedimiento');
  }

}
