import { CategoriaproductoService } from './../../../_service/categoriaproducto.service';
import { ProveedorService } from './../../../_service/proveedor.service';
import { ProductoService } from './../../../_service/producto.service';
import { CategoriaProducto } from './../../../_model/categoriaproducto';
import { Proveedor } from 'src/app/_model/proveedor';
import { Producto } from './../../../_model/producto';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogproducto',
  templateUrl: './dialogproducto.component.html',
  styleUrls: ['./dialogproducto.component.css']
})
export class DialogproductoComponent implements OnInit {

  producto: Producto;
  proveedores: Proveedor[] = []; 
  categorias: CategoriaProducto[] = [];

  idProveedorSeleccionado: number;
  idCategoriaSeleccionado: number;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  
   constructor(public dialogRef: MatDialogRef<DialogproductoComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Producto,
              private productoService:ProductoService,
              private proveedorService:ProveedorService,
              private categoriaproductoService:CategoriaproductoService) { }

   ngOnInit() {
      this.listarProveedores();
      this.listarCategorias();
      this.producto = new Producto();
      this.producto.idproducto=this.data.idproducto;
      this.producto.nombreproducto = this.data.nombreproducto;
      this.producto.unidadproducto = this.data.unidadproducto;
      this.producto.cantidadproducto = this.data.cantidadproducto;
      this.producto.pingresoproducto = this.data.pingresoproducto;
      this.producto.fingresoproducto = this.data.fingresoproducto;
      this.producto.marcaproducto = this.data.marcaproducto;
      this.producto.proveedor = this.data.proveedor;
      this.producto.categoriaproducto = this.data.categoriaproducto;
      if(this.data.idproducto!= null){
        this.idProveedorSeleccionado=this.data.proveedor.idproveedor;
        this.idCategoriaSeleccionado=this.data.categoriaproducto.idcategoria;
      }
    }


    listarProveedores() {
      this.proveedorService.listarProveedor().subscribe(data => {
        this.proveedores = data;
      });
    }
    listarCategorias() {
      this.categoriaproductoService.listarCategoriaProductol().subscribe(data => {
        this.categorias = data;
      });
    }
 
    operar(){
      
      if(this.producto != null && this.producto.idproducto > 0){

        let proveedor = new Proveedor();
        proveedor.idproveedor= this.idProveedorSeleccionado;
        let categoria = new CategoriaProducto();
        categoria.idcategoria= this.idCategoriaSeleccionado;


        this.producto.proveedor=proveedor;
        this.producto.categoriaproducto=categoria;


        this.productoService.modificarProducto(this.producto).subscribe(data => {
          
            this.productoService.listarProducto().subscribe(productos => {
              this.productoService.productoCambio.next(productos);
              this.productoService.mensaje.next("Se modificó correctamente");
            });
          
        });
      }else{

        let proveedor = new Proveedor();
        proveedor.idproveedor= this.idProveedorSeleccionado;
        let categoria = new CategoriaProducto();
        categoria.idcategoria= this.idCategoriaSeleccionado;

        this.producto.proveedor=proveedor;
        this.producto.categoriaproducto=categoria;


        if (this.producto.nombreproducto!=null&&this.producto.cantidadproducto!=null&&this.producto.pingresoproducto!=null
          &&this.producto.marcaproducto!=null&&this.producto.fingresoproducto!=null&&this.producto.proveedor!=null
          &&this.producto.categoriaproducto!=null) {
          
        this.productoService.registrarProducto(this.producto).subscribe(data => {
          
          this.productoService.listarProducto().subscribe(productos => {
            this.productoService.productoCambio.next(productos);
            this.productoService.mensaje.next("Se registró correctamente");
          });
        
      });
      this.dialogRef.close();

        }else{

          this.productoService.mensaje.next('Falta algún dato requerido del producto');
        }

      }

    }
  
    cancelar(){
      this.dialogRef.close();
      this.productoService.mensaje.next('Se canceló el procedimiento');
    }
    
}
