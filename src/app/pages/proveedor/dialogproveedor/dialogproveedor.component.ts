import { ProveedorService } from './../../../_service/proveedor.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Proveedor } from 'src/app/_model/proveedor';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialogproveedor',
  templateUrl: './dialogproveedor.component.html',
  styleUrls: ['./dialogproveedor.component.css']
})
export class DialogproveedorComponent implements OnInit {
  proveedor: Proveedor;
  constructor(public dialogRef: MatDialogRef<DialogproveedorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Proveedor,
    private proveedorService:ProveedorService) { }

  ngOnInit() {

    this.proveedor = new Proveedor();
      this.proveedor.idproveedor=this.data.idproveedor;
      this.proveedor.razonsocialproveedor = this.data.razonsocialproveedor;
      this.proveedor.descripcionproveedor = this.data.descripcionproveedor;
      this.proveedor.telefonoproveedor = this.data.telefonoproveedor;
      this.proveedor.direccionproveedor = this.data.direccionproveedor;
      this.proveedor.rucproveedor = this.data.rucproveedor;
      this.proveedor.correoproveedor = this.data.correoproveedor;
      if(this.data.idproveedor!= null){

      }
     
  }
operar(){
  
    if(this.proveedor != null && this.proveedor.idproveedor > 0){

      this.proveedorService.modificarProveedor(this.proveedor).subscribe(data => {
        
          this.proveedorService.listarProveedor().subscribe(proveedors => {
            this.proveedorService.proveedorCambio.next(proveedors);
            this.proveedorService.mensaje.next("Se modificó correctamente");
          });
        
      });
    }else{
      this.proveedorService.registrarProveedor(this.proveedor).subscribe(data => {
        
          this.proveedorService.listarProveedor().subscribe(proveedors => {
            this.proveedorService.proveedorCambio.next(proveedors);
            this.proveedorService.mensaje.next("Se registró correctamente");
          });
        
      });
    }
    this.dialogRef.close();
}

  cancelar(){
    this.dialogRef.close();
    this.proveedorService.mensaje.next('se canceló el procedimiento');
  }

}
