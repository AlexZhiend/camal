import { DialogproveedorComponent } from './dialogproveedor/dialogproveedor.component';
import { ProveedorService } from './../../_service/proveedor.service';
import { MatTableDataSource, MatSnackBar, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { Proveedor } from './../../_model/proveedor';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.component.html',
  styleUrls: ['./proveedor.component.css']
})
export class ProveedorComponent implements OnInit {

  lista: Proveedor[]=[];
  displayedColumns=['idproveedor','razonsocialproveedor','telefonoproveedor','rucproveedor','acciones'];
  dataSource: MatTableDataSource<Proveedor>;
  elim:Proveedor;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;

  constructor(private proveedorService:ProveedorService,public dialog:MatDialog,
    private matSnackBar:MatSnackBar,public route:ActivatedRoute) {
   }

   ngOnInit() {
    this.proveedorService.proveedorCambio.subscribe(data => {
      this.lista=data;
      this.dataSource= new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;

      this.proveedorService.mensaje.subscribe(data => {
        this.matSnackBar.open(data, 'Aviso',{duration:2000});
      });
    });

    this.proveedorService.listarProveedor().subscribe(data => {
      this.lista=data;
      console.log(data);
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });

  }

  applyFilter(filterValue: string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource.filter=filterValue;
  }
  
  
  openDialog(proveedor: Proveedor): void {

    let pro = proveedor != null ? proveedor : new Proveedor();
    let dialogRef = this.dialog.open(DialogproveedorComponent, {
      width: '500px',   
      disableClose: false,   
      data: pro      
    });
  }

  eliminar(){
    if (this.elim!=null && this.elim.idproveedor>0) {
      this.proveedorService.eliminarProveedor(this.elim.idproveedor).subscribe(data=>{
        this.proveedorService.mensaje.next('El proveedor se eliminó correctamente');
        this.proveedorService.listarProveedor().subscribe(data =>{
          this.lista = data;
          this.dataSource = new MatTableDataSource(this.lista);
        });
      });
    }
    else{
      this.proveedorService.mensaje.next('Ocurrió un error en el procedimiento')
    }
  }
  
  seleccionar(prov:Proveedor){
    this.elim=prov;
  }
  
}
