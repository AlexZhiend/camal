import { CategoriaAnimal } from 'src/app/_model/categoriaanimal';
import { Observable } from 'rxjs';
import { CategoriaanimalService } from 'src/app/_service/categoriaanimal.service';
import { ClienteService } from './../../../_service/cliente.service';
import { AnimalService } from './../../../_service/animal.service';
import { Cliente } from './../../../_model/cliente';
import { Animal } from './../../../_model/animal';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatAutocompleteSelectedEvent } from '@angular/material';
import { FormControl, FormGroup } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-dialoganimal',
  templateUrl: './dialoganimal.component.html',
  styleUrls: ['./dialoganimal.component.css']
})
export class DialoganimalComponent implements OnInit {

  animal: Animal;
  clientes: Cliente[] = []; 
  categorias: CategoriaAnimal[] = [];

  cliente = new FormControl();
  categoria = new FormControl();

  form:FormGroup;
  form1:FormGroup;
  
  filteredOptions: Observable<Cliente[]>;
  filteredOptions1: Observable<CategoriaAnimal[]>;
  clienteseleccionado: Cliente;
  categoriaseleccionado: CategoriaAnimal;

  idClienteSeleccionado: number;
  idCategoriaSeleccionado: number;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  
   constructor(public dialogRef: MatDialogRef<DialoganimalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Animal,
              private animalService:AnimalService,
              private clienteService:ClienteService,
              private categoriaAnimalService:CategoriaanimalService) {

                this.form=new FormGroup({
                  'cliente': new FormControl(null),
                });
                this.form1=new FormGroup({
                  'categoria': new FormControl(null),
                });

               }

   ngOnInit() {
      this.listarClientes();
      this.listarCategorias();
      this.animal = new Animal();
      this.animal.idanimal=this.data.idanimal;
      this.animal.especie = this.data.especie;
      this.animal.descripcion = this.data.descripcion;
      this.animal.sexo = this.data.sexo;
      this.animal.cantidadanimal = this.data.cantidadanimal;
      this.animal.fingresoanimal = this.data.fingresoanimal;
      this.animal.estado=this.data.estado;
      if(this.data.idanimal!= null){
        this.cliente.setValue(this.data.cliente);
        this.categoria.setValue(this.data.categoriaanimal);
      }


      
    this.filteredOptions = this.cliente.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombresyapellidos),
      map(nombresyapellidos => nombresyapellidos ? this._filter(nombresyapellidos) : this.clientes.slice())
    );

    this.filteredOptions1 = this.categoria.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombresCategoriaAnimal),
      map(nombresCategoriaAnimal => nombresCategoriaAnimal ? this._filter1(nombresCategoriaAnimal) : this.categorias.slice())
    );
  
  }


    listarClientes() {
      this.clienteService.listarCliente().subscribe(data => {
        this.clientes = data;
      });
    }
    listarCategorias() {
      this.categoriaAnimalService.listarCategoriaAnimal().subscribe(data => {
        this.categorias = data;
      });
    }


    displayFn(cliente?: Cliente): string | undefined {
      return cliente ? cliente.apellidosynombres : undefined;
    }
  
    displayFn1(categoriaAnimal?: CategoriaAnimal): string | undefined {
      return categoriaAnimal ? categoriaAnimal.denominacion : undefined;
    }
  
    private _filter(apellidosynombres: string): Cliente[] {
      const filterValue = apellidosynombres.toLowerCase();
      return this.clientes.filter(cliente => cliente.apellidosynombres.toLowerCase().indexOf(filterValue) === 0);
    }
  
    private _filter1(denominacion: string): CategoriaAnimal[] {
      const filterValue = denominacion.toLowerCase();
      return this.categorias.filter(categoria => categoria.denominacion.toLowerCase().indexOf(filterValue) === 0);
    }
  
  
    onSelectionChanged(event: MatAutocompleteSelectedEvent) {
      this.clienteseleccionado = event.option.value;
      this.idClienteSeleccionado = this.clienteseleccionado.idcliente;
      console.log(this.clienteseleccionado)
    }
  
    onSelectionChanged1(event: MatAutocompleteSelectedEvent) {
      this.categoriaseleccionado = event.option.value;
      this.idCategoriaSeleccionado=this.categoriaseleccionado.idcategoriaanimal;
      console.log(this.categoriaseleccionado)
    }
 
    operar(){
      
      if(this.animal != null && this.animal.idanimal > 0){

        let cliente = new Cliente();
        cliente.idcliente= this.data.cliente.idcliente;
        let categoria = new CategoriaAnimal();
        categoria.idcategoriaanimal= this.data.categoriaanimal.idcategoriaanimal;


        this.animal.cliente=cliente;
        this.animal.categoriaanimal=categoria;

        console.log(this.animal)

        this.animalService.modificarAnimal(this.animal).subscribe(data => {
          
            this.animalService.listarAnimal().subscribe(Animals => {
              this.animalService.animalCambio.next(Animals);
              this.animalService.mensaje.next("Se modificó correctamente");
            });
            this.dialogRef.close();
        });
      }else{

        let cliente = new Cliente();
        cliente.idcliente= this.idClienteSeleccionado;
        let categoria = new CategoriaAnimal();
        categoria.idcategoriaanimal= this.idCategoriaSeleccionado;


        this.animal.cliente=cliente;
        this.animal.categoriaanimal=categoria;
        this.animal.estado=1;


        if (this.animal.especie!=null) {
          console.log(this.animal);
        this.animalService.registrarAnimal(this.animal).subscribe(data => {
          
          this.animalService.listarAnimal().subscribe(Animals => {
            this.animalService.animalCambio.next(Animals);
            this.animalService.mensaje.next("Se registró correctamente");
          });
        
      });
      this.dialogRef.close();

        }else{

          this.animalService.mensaje.next('Falta algún dato requerido del Animal');
        }

      }

    }
  
    cancelar(){
      this.dialogRef.close();
      this.animalService.mensaje.next('Se canceló el procedimiento');
    }
    
}
