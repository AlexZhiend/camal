import { DialoganimalComponent } from './dialoganimal/dialoganimal.component';
import { Cliente } from './../../_model/cliente';
import { ClienteService } from './../../_service/cliente.service';
import { AnimalService } from './../../_service/animal.service';
import { MatTableFilter } from 'mat-table-filter';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { Animal } from './../../_model/animal';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoriaanimalService } from 'src/app/_service/categoriaanimal.service';
import { CategoriaAnimal } from 'src/app/_model/categoriaanimal';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent implements OnInit {

  animales: Animal[] = [];
  displayedColumns = ['especie', 'cantidadanimal', 'descripcion','Categoria','apellidosynombres','fingresoanimal','acciones'];
  dataSource: MatTableDataSource<Animal>;
  mensaje: string;
  his:any='';
  hist:Animal;
  filterEntity: Animal;
  filterType: MatTableFilter;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private categoriaanimalService:CategoriaanimalService,
    private clienteService:ClienteService,
              private AnimalService:AnimalService,    
              public dialog:MatDialog, 
              public snackBar:MatSnackBar
              ) {

   }

  ngOnInit() {
    this.AnimalService.animalCambio.subscribe(data => {
      this.animales = data;
      this.dataSource = new MatTableDataSource(this.animales);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.AnimalService.mensaje.subscribe(data => {
      console.log(data);
      this.snackBar.open(data, null, { duration: 2000 });
    });

    this.AnimalService.listarAnimal().subscribe(data => {
      this.animales = data;
      this.dataSource = new MatTableDataSource(this.animales);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

          
    this.filterEntity = new Animal();
    this.filterEntity.cliente = new Cliente();
    this.filterEntity.categoriaanimal = new CategoriaAnimal();
    this.filterType = MatTableFilter.ANYWHERE;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  openDialog(animal: Animal): void {
    let pro = animal != null ? animal : new Animal();
    let dialogRef = this.dialog.open(DialoganimalComponent, {
      width: '700px',   
      disableClose: false,   
      
      data: pro      
    });
  }


}
