import { CategoriaAnimal } from 'src/app/_model/categoriaanimal';
import { Animal } from './../../_model/animal';
import { AnimalService } from './../../_service/animal.service';
import { ClienteService } from './../../_service/cliente.service';
import { Observable } from 'rxjs';
import { Cliente } from './../../_model/cliente';
import { ComprobanteService } from './../../_service/comprobante.service';
import { Comprobante } from './../../_model/comprobante';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent, MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { DetalleSacrificio } from 'src/app/_model/detallesacrificio';
import * as moment from 'moment';


@Component({
  selector: 'app-comprobante',
  templateUrl: './comprobante.component.html',
  styleUrls: ['./comprobante.component.css']
})
export class ComprobanteComponent implements OnInit {
  comprobantepago:Comprobante;
  form: FormGroup;

  cliente = new FormControl();
  clientes: Cliente[] = [];
  filteredOptions: Observable<Cliente[]>;
  clienteseleccionado: Cliente;
  clienteseleccionado1: Cliente;
  idClienteSeleccionado: number;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  numerocomprobanteu:string;
  ultimocomprobante=new Comprobante();

  form3:FormGroup;
  form2:FormGroup;
  formb:FormGroup;

  //datasource
  animales:Animal[] = [];
  displayedColumns = ['especie', 'cantidadanimal','Categoria','acciones'];
  dataSource:MatTableDataSource<Animal>;

  //datasource1
  dataSource1:MatTableDataSource<DetalleSacrificio>;
  displayedColumns1 = ['cantidaddetalle', 'denominacionexamen','precioexamen','importedetalle','acciones'];

  //boletas
  comprobantes: Comprobante[] = [];
  dataSource2:MatTableDataSource<Comprobante>;
  displayedColumns2 = ['apellidosynombres', 'numerocomprobante','fecha','deuda','acciones'];



  //detalle
  detalles:DetalleSacrificio[]=[];
  precioCategoria=new CategoriaAnimal();
  animalSeleccionado= new Animal();
  detalleseleccionado=new DetalleSacrificio();
  total:number;

  //pdfs
  cp:any='';
  his:any='';
  at:any='';

  comp:Comprobante;

  //fechas
  fechaSeleccionada3: Date = null;
  fechaSeleccionada4: Date = null;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort) sort:MatSort;


  constructor(private comprobanteService:ComprobanteService,
              private clienteService:ClienteService,
              private animalService:AnimalService, public snackBar:MatSnackBar) {

               this.comprobantepago=new Comprobante();
                this.form= new FormGroup({
                  'idcomprobante': new FormControl(0),
                  'cliente':new FormControl(null),
                  'ruc': new FormControl(null),
                  'descripcionotros': new FormControl(null),
                  'otros':new FormControl(0.0),
                });
                this.detalles=[];
                this.form3= new FormGroup({
                  'cantidaddetalle': new FormControl(0),
                });
                this.form2= new FormGroup({
                  'cancelado':new FormControl(0.0)
                });
                this.formb= new FormGroup({
                  'cliente':new FormControl(0.0)
                });
   }

  ngOnInit() {
    this.companterior();
    this.listarClientes();
    this.listarComprobantes();


    this.filteredOptions = this.cliente.valueChanges.pipe(
      startWith(''),
      map(value => typeof value === 'string' ? value : value.nombresyapellidos),
      map(nombresyapellidos => nombresyapellidos ? this._filter(nombresyapellidos) : this.clientes.slice())
    );

    this.comprobanteService.mensaje.subscribe(data=>{
      this.snackBar.open(data, null, { duration: 3000 });
      })
  }

  listarClientes() {
    this.clienteService.listarCliente().subscribe(data => {
      this.clientes = data;
    });
  }

  listarComprobantes(){
    this.comprobanteService.listarComprobante().subscribe(data=>{
      this.comprobantes=data;
      this.dataSource2=new MatTableDataSource(this.comprobantes);
      this.dataSource2.paginator=this.paginator;
      this.dataSource2.sort=this.sort;
    })
  }

  applyFilter(filterValue: string){
    filterValue=filterValue.trim();
    filterValue=filterValue.toLowerCase();
    this.dataSource2.filter=filterValue;
  }

  displayFn(cliente?: Cliente): string | undefined {
    return cliente ? cliente.apellidosynombres : undefined;
  }

  private _filter(apellidosynombres: string): Cliente[] {
    const filterValue = apellidosynombres.toLowerCase();
    return this.clientes.filter(cliente => cliente.apellidosynombres.toLowerCase().indexOf(filterValue) === 0);
  }

  onSelectionChanged(event: MatAutocompleteSelectedEvent) {
    this.clienteseleccionado = event.option.value;
    this.idClienteSeleccionado = this.clienteseleccionado.idcliente;
    this.animalService.listarAnimalXCliente(this.idClienteSeleccionado).subscribe(data=>{
      this.animales=data;
      this.dataSource= new MatTableDataSource(this.animales);
    });

    console.log(this.clienteseleccionado)
  }

  onSelectionChanged1(event: MatAutocompleteSelectedEvent) {
    this.clienteseleccionado1 = event.option.value;
    console.log(this.clienteseleccionado1)
  }


  agregar(){
    if (this.form3.valid===true&&this.animalSeleccionado.especie!=null) {
      if (this.animalSeleccionado.cantidadanimal>=this.form3.value['cantidaddetalle']) {
        let det = new DetalleSacrificio();

        det.cantidad= this.form3.value['cantidaddetalle'];
        det.animal= this.animalSeleccionado;
        det.importe=det.cantidad*det.animal.categoriaanimal.precio;

        this.detalles.push(det);
        this.dataSource1 = new MatTableDataSource(this.detalles);
        this.form3.reset();
        this.animalSeleccionado.cantidadanimal=this.animalSeleccionado.cantidadanimal-det.cantidad
      }
      else{
        this.comprobanteService.mensaje.next("La cantidad igresada excede al stock del animal del cliente");
      }
    }else{
      this.comprobanteService.mensaje.next("Falta algún dato requerido de la venta");
    }
  }

  addanimal(anim:Animal){
    this.animalSeleccionado=anim;
    console.log(this.animalSeleccionado)
  }

  eliminar(detalle:DetalleSacrificio){
    var indice=this.detalles.indexOf(detalle);
    this.detalles.splice(indice, 1);
    this.dataSource1 = new MatTableDataSource(this.detalles);
    this.detalleseleccionado=detalle;
    this.animalSeleccionado=detalle.animal;
    this.animalSeleccionado.cantidadanimal=this.animalSeleccionado.cantidadanimal+detalle.cantidad;
    this.dataSource=new MatTableDataSource(this.animales);
  }



  operar(){
    if (this.form.valid ===true && this.clienteseleccionado!=null) {
    this.comprobantepago.fecha=this.fechaSeleccionada;
    this.comprobantepago.cliente = this.clienteseleccionado.apellidosynombres;
    this.comprobantepago.descripcionotros=this.form.value['descripcionotros'];
    this.comprobantepago.numerocomprobante=parseInt(this.numerocomprobanteu);
    this.comprobantepago.ruc=this.form.value['ruc'];
    this.comprobantepago.otros=this.form.value['otros'];
    this.comprobantepago.cancelado=this.form2.value['cancelado'];
    this.comprobantepago.total=this.total;
    this.comprobantepago.deuda=this.total-this.form2.value['cancelado'];
    this.comprobantepago.detallesacrificio=this.detalles;
    
      this.comprobanteService.registrarComprobante(this.comprobantepago).subscribe(data=>{
        this.comprobanteService.mensaje.next("Se realizó el registro con éxito");

        for(let i=0; i<this.detalles.length;i++){
          this.animalService.listarAnimalId(this.detalles[i].animal.idanimal).subscribe(data=>{
            data.cantidadanimal=data.cantidadanimal-this.detalles[i].cantidad;
            if (data.cantidadanimal==0) {
              data.estado=0;
            }
            this.animalService.modificarAnimal(data).subscribe(dataa=>{
            });
          });
        }
      });
    }else{
      this.comprobanteService.mensaje.next("Falta algún dato requerido del recibo");
    }




  }


  cancelar(){
    this.comprobanteService.mensaje.next('Se canceló el procedimiento');
    window.location.reload();
  }

  getTotalCost() {
    this.total=this.detalles.map(t => t.importe).reduce((acc, value) => acc + value, 0)+this.form.value['otros'];
    return this.detalles.map(t => t.importe).reduce((acc, value) => acc + value, 0)+this.form.value['otros'];
  }

  companterior(){
    this.comprobanteService.listarComprobantePagoId().subscribe(data=>{
      this.ultimocomprobante=data;
      if(this.ultimocomprobante!=null){
      var numero= this.ultimocomprobante.numerocomprobante + 1;
      }else{
        numero=1;
      }

      let numeronuevo=numero.toString();
      let cero = "0";
      for(var _i=0;_i < 7-numeronuevo.length; _i++){
        cero=cero+"0";
      }
      this.numerocomprobanteu=cero+numeronuevo;
    });
  }

  estadoBotonAgregar(){
    if(this.form3.value['cantidaddetalle']==0){
      return true
    }
    else{
      return(this.animalSeleccionado.cantidadanimal<this.form3.value['cantidaddetalle']);
    };
  }

  pdf(){
    this.comprobanteService.generarRecibo(parseInt(this.numerocomprobanteu)).subscribe(data=>{
      this.cp=data;
    });
  }

  pdf2(){
    if(this.clienteseleccionado1!=null){
      this.comprobanteService.generarHistorial(this.clienteseleccionado1.apellidosynombres).subscribe(data=>{
        this.his=data;
      });
    }else{
      this.comprobanteService.mensaje.next('Falta seleccionar el dato del cliente')
    }

  }

  pdfAtencion(){
    if (this.fechaSeleccionada3!=null && this.fechaSeleccionada4!=null 
      && this.fechaSeleccionada3<this.fechaSeleccionada4) {
        let s1 = moment(this.fechaSeleccionada3).format("DD-MM-YYYY");
        let s2 = moment(this.fechaSeleccionada4).format("DD-MM-YYYY");
        this.comprobanteService.reporteServicios(s1,s2).subscribe(data=>{
          this.at=data;
        })
    }else{
      this.comprobanteService.mensaje.next('La fecha de inicio debe ser mayor a la fecha de fin');
    }
  }

 seleccionar(row:Comprobante){
  this.comp=row;
 }

  cancelarrecibo(){
    this.comp.deuda=0;
    this.comp.cancelado=this.comp.total;
    this.comprobanteService.modificarComprobante(this.comp).subscribe(data=>{
      this.comprobanteService.mensaje.next('La operación se realizó satisfactoriamente');
      this.listarComprobantes();
    })
  }

}
