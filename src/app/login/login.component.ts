import { TOKEN_NAME } from './../_shared/var.constant';
import { MenuService } from './../_service/menu.service';
import { LoginService } from './../_service/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  usuario: string;
  clave: string;

  constructor(private loginService: LoginService, private router: Router,  private menuService: MenuService) { }

  ngOnInit() {
  }

  iniciarSesion(){
    this.loginService.login(this.usuario, this.clave).subscribe(data=>{
      //console.log(data);
      if (data) {
        let token = JSON.stringify(data);
        sessionStorage.setItem(TOKEN_NAME, token);

        let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
        const decodedToken = decode(tk.access_token);
        console.log(decodedToken);

        this.menuService.listarPorUsuario(decodedToken.user_name).subscribe(data => {
          this.menuService.menuCambio.next(data);
        });

        console.log(decodedToken.authorities);
        let roles = decodedToken.authorities;
        for (let i = 0; roles.length; i++) {
          let rol = roles[i];
          if (rol === 'Administrador') {
            this.router.navigate(['cliente']);
            break;  
          }else{
            if (rol === 'Caja') {
              this.router.navigate(['comprobante']);
              break;  
            }else{
              if (rol === 'Almacen') {
                this.router.navigate(['producto']);
                break;  
              }else{
                if (rol === 'Registrador') {
                  this.router.navigate(['animal']);
                  break;  
                }else{
                  this.router.navigate(['cliente']);
                  break;
                }
              }
            }
          }
        }
      }

    });


  }

}
