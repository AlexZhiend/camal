import { ComprobanteComponent } from './pages/comprobante/comprobante.component';
import { Not403Component } from './pages/not403/not403.component';
import { LoginComponent } from './login/login.component';
import { ProveedorComponent } from './pages/proveedor/proveedor.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { AnimalComponent } from './pages/animal/animal.component';
import { GuardService } from './_service/guard.service';
import { CategoriaproductoComponent } from './pages/categoriaproducto/categoriaproducto.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriaanimalComponent } from './pages/categoriaanimal/categoriaanimal.component';

const routes: Routes = [
  {path:'categoriaproducto', component: CategoriaproductoComponent,canActivate:[GuardService]},
  {path:'animal', component: AnimalComponent,canActivate:[GuardService]},
  {path:'categoriaanimal', component: CategoriaanimalComponent,canActivate:[GuardService]},
  {path:'cliente', component: ClienteComponent,canActivate:[GuardService]},
  {path:'comprobante', component: ComprobanteComponent,canActivate:[GuardService]},
  {path:'producto', component: ProductoComponent,canActivate:[GuardService]},
  {path:'proveedor', component: ProveedorComponent,canActivate:[GuardService]},
  {path:"login", component: LoginComponent},
  {path:'not-403', component: Not403Component },
  {path: "", redirectTo:'login', pathMatch:'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
