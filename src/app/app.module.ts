import { ServerErrorsInterceptor } from './_shared/server-errors.interceptor';
import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrubaComponent } from './pages/pruba/pruba.component';
import { MatTableFilterModule } from 'mat-table-filter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AnimalComponent } from './pages/animal/animal.component';
import { CategoriaanimalComponent } from './pages/categoriaanimal/categoriaanimal.component';
import { CategoriaproductoComponent } from './pages/categoriaproducto/categoriaproducto.component';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { ComprobanteComponent } from './pages/comprobante/comprobante.component';
import { ProductoComponent } from './pages/producto/producto.component';
import { ProveedorComponent } from './pages/proveedor/proveedor.component';
import { Not403Component } from './pages/not403/not403.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { DialoganimalComponent } from './pages/animal/dialoganimal/dialoganimal.component';
import { DialogcategoriaanimalComponent } from './pages/categoriaanimal/dialogcategoriaanimal/dialogcategoriaanimal.component';
import { DialogcategoriaproductoComponent } from './pages/categoriaproducto/dialogcategoriaproducto/dialogcategoriaproducto.component';
import { DialogclienteComponent } from './pages/cliente/dialogcliente/dialogcliente.component';
import { DialogproductoComponent } from './pages/producto/dialogproducto/dialogproducto.component';
import { DialogproveedorComponent } from './pages/proveedor/dialogproveedor/dialogproveedor.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';

@NgModule({
  declarations: [
    AppComponent,
    PrubaComponent,
    LoginComponent,
    AnimalComponent,
    CategoriaanimalComponent,
    CategoriaproductoComponent,
    ClienteComponent,
    ComprobanteComponent,
    ProductoComponent,
    ProveedorComponent,
    Not403Component,
    DialoganimalComponent,
    DialogcategoriaanimalComponent,
    DialogcategoriaproductoComponent,
    DialogclienteComponent,
    DialogproductoComponent,
    DialogproveedorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,MaterialModule,MatTableFilterModule,FormsModule,HttpClientModule,
    ReactiveFormsModule,NgxExtendedPdfViewerModule
  ], 
  entryComponents:[DialoganimalComponent,DialogcategoriaanimalComponent,DialogcategoriaproductoComponent,
  DialogclienteComponent,DialogproductoComponent,DialogproveedorComponent],
  providers: [{provide:LocationStrategy, useClass: HashLocationStrategy},{
    provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorsInterceptor,
      multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
